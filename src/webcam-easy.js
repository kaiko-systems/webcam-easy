export default class Webcam {

  constructor(webcamElement, facingMode = 'user', canvasElement = null, snapSoundElement = null) {
    this._webcamElement = webcamElement;
    this._webcamElement.width = this._webcamElement.width || screen.availWidth;
    this._webcamElement.height = this._webcamElement.height || screen.availHeight;
    this._facingMode = facingMode;
    this._webcamList = [];
    this._streamList = [];
    this._selectedDeviceId = '';
    this._canvasElement = canvasElement;
    this._snapSoundElement = snapSoundElement;
    this._isCameraNeeded = false;
    this._resizeTheWithTheWindowBinded = () => this._resizeTheWithTheWindow();
  }

  _resizeTheWithTheWindow() {
    this._webcamElement.width = screen.availWidth;
    this._webcamElement.height = screen.availHeight;
    // Wait for screen to finish resizing.
    setTimeout(
      () => {
        if (this._isCameraNeeded) {
          this.start();
        }
      },
      1000
    )
  }

  get facingMode() {
    return this._facingMode;
  }

  set facingMode(value) {
    this._facingMode = value;
  }

  get webcamList() {
    return this._webcamList;
  }

  get webcamCount() {
    return this._webcamList.length;
  }

  get selectedDeviceId() {
    return this._selectedDeviceId;
  }

  /* Get all video input devices info */
  getVideoInputs(mediaDevices) {
    this._webcamList = [];
    mediaDevices.forEach(mediaDevice => {
      if (mediaDevice.kind === 'videoinput') {
        this._webcamList.push(mediaDevice);
      }
    });
    if (this._webcamList.length == 1) {
      this._facingMode = 'user';
    }
    return this._webcamList;
  }

  /* Get media constraints */
  getMediaConstraints() {
    var videoConstraints = {};
    if (this._selectedDeviceId == '') {
      videoConstraints.facingMode = this._facingMode;
    } else {
      videoConstraints.deviceId = { exact: this._selectedDeviceId };
    }
    /* Try to ideally get a media stream that takes at least 1080p pictures */
    var isPortraitInPortraitMode = (
      // make this work in safari
      window.matchMedia("(orientation: portrait)").matches ||
      window.screen.orientation && screen.orientation.type.indexOf("portrait") > -1
    );
    if (isPortraitInPortraitMode)
      videoConstraints.height = { ideal: 1080 };
    else
      videoConstraints.width = { ideal: 1080 };
    var constraints = {
      video: videoConstraints,
      audio: false
    };
    return constraints;
  }

  /* Select camera based on facingMode */
  selectCamera() {
    for (let webcam of this._webcamList) {
      let label = webcam.label.toLowerCase()
      if ((this._facingMode == 'user' && label.includes('front'))
        || (this._facingMode == 'environment' && (
          label.includes('back') ||
          label.includes('rear')
        ))
      ) {
        console.log("Selected", webcam.label)
        this._selectedDeviceId = webcam.deviceId;
        break;
      }
    }
  }

  /* Change Facing mode and selected camera */
  flip() {
    this._facingMode = (this._facingMode == 'user') ? 'enviroment' : 'user';
    this._webcamElement.style.transform = "";
    this.selectCamera();
  }

  /*
    1. Get permission from user
    2. Get all video input devices info
    3. Select camera based on facingMode
    4. Start stream
  */
  async start(startStream = true) {
    this._isCameraNeeded = true;
    window.addEventListener("resize", this._resizeTheWithTheWindowBinded);
    let stream = await navigator.mediaDevices.getUserMedia(
      this.getMediaConstraints()
    );
    this._streamList.push(stream);
    await this.info();
    this.selectCamera();
    if (startStream) {
      await this.stream();
      if (!this._isCameraNeeded) {
        this.stop();
      }
      return this._facingMode;
    } else {
      if (!this._isCameraNeeded) {
        this.stop();
      }
      return this._selectedDeviceId;
    }
  }

  /* Get all video input devices info */
  async info() {
    let devices = await navigator.mediaDevices.enumerateDevices();
    this.getVideoInputs(devices);
    return this._webcamList;
  }

  /* Start streaming webcam to video element */
  async stream() {
    let stream = await navigator.mediaDevices.getUserMedia(
      this.getMediaConstraints()
    );
    this._streamList.push(stream);
    this._webcamElement.srcObject = stream;
    let self = this;
    this._webcamElement.onloadedmetadata = function () {
      self.snapHeight = this.videoHeight;
      self.snapWidth = this.videoWidth;
    };
    if (this._facingMode == 'user') {
      this._webcamElement.style.transform = "scale(-1,1)";
    }
    await this._webcamElement.play();
    return this._facingMode;
  }

  /* Stop streaming webcam */
  stop() {
    window.removeEventListener("resize", this._resizeTheWithTheWindowBinded);
    this._isCameraNeeded = false;
    this._webcamElement.pause();
    this._streamList.forEach(stream => {
      stream.getTracks().forEach(track => {
        track.stop();
      });
    });
  }

  snap() {
    if (this._canvasElement != null) {
      if (this._snapSoundElement != null) {
        this._snapSoundElement.play();
      }
      this._canvasElement.width = this.snapWidth || this._webcamElement.scrollWidth;
      this._canvasElement.height = this.snapHeight || this._webcamElement.scrollHeight;
      let context = this._canvasElement.getContext('2d');
      if (this._facingMode == 'user') {
        context.translate(this._canvasElement.width, 0);
        context.scale(-1, 1);
      }
      context.clearRect(0, 0, this._canvasElement.width, this._canvasElement.height);
      context.drawImage(this._webcamElement, 0, 0, this._canvasElement.width, this._canvasElement.height);
    }
    else {
      throw "canvas element is missing";
    }
  }
}
